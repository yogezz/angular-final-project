import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListComponent } from './list/list.component';
import { DashboardComponent } from './dashboard.component';
import { CreateComponent } from './create/create.component';

import { AssignComponent } from './assign/assign.component';
import { UserRegComponent } from './user-reg/user-reg.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: [
      {
        path: 'create',
        component: CreateComponent,
      },

      {
        path: 'assign',
        component: AssignComponent,
      },
      {
        path: 'list',
        component: ListComponent,
      },
      {
        path: 'user-reg',
        component: UserRegComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardRoutingModule {}
