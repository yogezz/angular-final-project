import { Component, ViewChild, createComponent } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { CreateService } from 'src/app/service/task.service';
import { MatDialog } from '@angular/material/dialog';
import { CreateComponent } from '../create/create.component';
import { CreateVoTs } from 'src/app/model/create-vo';

@Component({
  selector: 'app-assign',
  templateUrl: './assign.component.html',
  styleUrls: ['./assign.component.css'],
})
export class AssignComponent {
  constructor(
    private taskService: CreateService,
    private matDialog: MatDialog
  ) {}
  ngOnInit(): void {
    this.getTaskList();
  }
  displayedColumns: string[] = [
    'taskNumber',
    'taskDescription',
    'assign',
    'startingDate',
    'estimateDate',
    'taskStatus',
    'sprint',
    'action',
  ];
  dataSource!: MatTableDataSource<object>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  getTaskList() {
    this.taskService.getTaskList().subscribe({
      next: (res: any) => {
        this.dataSource = new MatTableDataSource(res);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      },
      error: console.log,
    });
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  updateTask(data: CreateVoTs) {
    this.matDialog.open(CreateComponent, { data });
  }

  deleteTask(id: number) {
    this.taskService.deleteTask(id).subscribe({
      next: (res) => {
        alert('Task Deleted Successfully');
        this.getTaskList();
      },
      error: console.log,
    });
  }
}
