import { DialogRef } from '@angular/cdk/dialog';
import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { RegisterService } from 'src/app/service/register.service';

@Component({
  selector: 'app-user-reg',
  templateUrl: './user-reg.component.html',
  styleUrls: ['./user-reg.component.css'],
})
export class UserRegComponent {
  userForm: FormGroup;
  role: number[] = [1, 2];
  constructor(
    private usr: FormBuilder,
    private regService: RegisterService,
    private router: Router,
    private dialogRef: DialogRef
  ) {
    this.userForm = this.usr.group({
      userFirstName: '',
      userLastName: '',
      userName: '',
      userPassword: '',
      role: '',
    });
  }
  onFormSubmit() {
    if (this.userForm.valid) {
      this.regService.addUser(this.userForm.value).subscribe(
        (val: any) => {
          alert('User Added Successfully');
          this.dialogRef.close();
        },
        (err: any) => {
          console.error(err);
        }
      );
    }
  }
  cancel() {
    this.dialogRef.close();
  }
}
