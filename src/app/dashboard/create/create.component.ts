import { Component, Inject, OnInit } from '@angular/core';
import { DialogRef } from '@angular/cdk/dialog';
import { FormBuilder, FormGroup } from '@angular/forms';
import { RegisterService } from 'src/app/service/register.service';
import { RegisterVo } from 'src/app/model/register-vo';
import { CreateService } from 'src/app/service/task.service';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css'],
})
export class CreateComponent implements OnInit {
  createForm: FormGroup;

  userVo: RegisterVo = new RegisterVo();

  status: String[] = [
    'New',
    'InProgress',
    'Resolved',
    'In-Dev-Resolved',
    'Closed',
  ];

  constructor(
    private create: FormBuilder,
    private userDetails: RegisterService,
    private dialogref: DialogRef,
    private createService: CreateService,

    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.createForm = this.create.group({
      taskDescription: '',
      taskNumber: '',
      assign: '',
      startingDate: '',
      estimateDate: '',
      taskStatus: '',
      sprint: '',
    });
  }
  ngOnInit(): void {
    this.getAllUser();
    this.createForm.patchValue(this.data);
  }

  user: any[] = [];

  getAllUser() {
    this.userDetails.getUser().subscribe(
      (val: any) => {
        val.map((a: any) => this.user.push(a.userName));
        console.log(val);
      },
      (err: any) => {
        console.error(err);
      }
    );
  }

  submit() {
    if (this.createForm.valid) {
      if (this.data) {
        const id = this.data.taskId;
        console.log(id);
        this.createService.updateTask(id, this.createForm.value).subscribe(
          (val: any) => {
            alert('Task Updated Successfully');
            this.dialogref.close();
          },
          (err: any) => {
            console.error(err);
          }
        );
      } else {
        this.createService.createTask(this.createForm.value).subscribe(
          (val: any) => {
            alert('Task Added Successfully');
            this.dialogref.close();
          },
          (err: any) => {
            console.error(err);
          }
        );
      }
    }
  }

  cancel() {
    this.dialogref.close();
  }
}
