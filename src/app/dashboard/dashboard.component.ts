import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { UserRegComponent } from './user-reg/user-reg.component';
import { CreateComponent } from './create/create.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent {
  router: any;
  constructor(private dialog: MatDialog) {}
  useradd() {
    this.dialog.open(UserRegComponent);
  }
  createTask() {
    this.dialog.open(CreateComponent);
  }
}
