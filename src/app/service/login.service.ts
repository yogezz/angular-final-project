import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LoginVo } from '../model/login-vo';
import { Observable } from 'rxjs';
import { UserAuth } from '../model/user-auth';

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  private baseurl = 'http://localhost:8080/authenticate';
  constructor(private httpClient: HttpClient) {}
  loginUser(user: LoginVo) {
    return this.httpClient.post<UserAuth>(`${this.baseurl}`, user);
  }
}
