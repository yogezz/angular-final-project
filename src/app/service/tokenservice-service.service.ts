import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';

import { Injectable, Injector } from '@angular/core';

import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class TokenserviceService implements HttpInterceptor {
  constructor() {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const accessToken = req.clone({
      setHeaders: {
        Authorization: 'Bearer ' + localStorage.getItem('accesstoken'),
      },
    });

    return next.handle(accessToken);
  }
}
