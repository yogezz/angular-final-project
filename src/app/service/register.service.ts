import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RegisterVo } from '../model/register-vo';

@Injectable({
  providedIn: 'root',
})
export class RegisterService {
  private baseurl = 'http://localhost:8080/registerNewUser';
  private baseurl1 = 'http://localhost:8080/getAllUser';
  constructor(private http: HttpClient) {}

  addUser(data: RegisterVo): Observable<Object> {
    return this.http.post(`${this.baseurl}`, data);
  }

  getUser(): Observable<Object> {
    return this.http.get(`${this.baseurl1}`);
  }
}
