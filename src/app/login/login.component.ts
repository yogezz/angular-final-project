import { Component } from '@angular/core';
import { LoginVo } from '../model/login-vo';
import { LoginService } from '../service/login.service';
import { Router, RouterLink } from '@angular/router';
import { UserAuth } from '../model/user-auth';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent {
  user: LoginVo = new LoginVo();
  UserAuthVo = new UserAuth();
  constructor(private loginservice: LoginService, private router: Router) {}
  login() {
    console.log(this.user);
    this.loginservice.loginUser(this.user).subscribe(
      (data) => {
        localStorage.setItem('roleName', data.user.role[0].roleName);
        localStorage.setItem('accesstoken', data.jwtToken);
        this.router.navigate(['/menu']);
      },
      (error) => {
        alert('Please enter valid username and password');
      }
    );
  }
}
